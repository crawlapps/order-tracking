<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    */

    'CYBERTONICA_AUTHORIZATION'  => env('CYBERTONICA_AUTHORIZATION', 'Basic c2FtOnlTbytMaEVUTWRySzFvUVExcU5aNEE0TkVVQmkvNDloOFpmSzFlQnJtT2c9Kys='),

    'CYBERTONICA_SERVER' => env('CYBERTONICA_SERVER', '46.22.128.95'),

    'CYBERTONICA_API_KEY'     => env('CYBERTONICA_API_KEY', '7qJezpwQdWFsD6r'),

    'CYBERTONICA_PYTHON_ORDER_ENDPOINT'   => env('CYBERTONICA_PYTHON_ORDER_ENDPOINT', 'https://shopify.cybertonica.com:7499/api/v2.2/events/'),

    'CYBERTONICA_ORDER_SUBCHANNEL'   => env('CYBERTONICA_ORDER_SUBCHANNEL', 'shopify_orders'),

    'CYBERTONICA_API_USER' => env('CYBERTONICA_API_USER', 'shopify'),

//    'GOOGLE_MAP_API_KEY' => 'AIzaSyCUitxAtTUyoAORlVdnkOkCxdKSDXSjLc4',
    'GOOGLE_MAP_API_KEY' =>  env('GOOGLE_MAP_API_KEY', 'AIzaSyBXCd3OLqbqIQIBZrnmEbUMQHPYSjq8VBE'),

    'DASHBOARD_ENDPOINT' => env('DASHBOARD_ENDPOINT')

];
