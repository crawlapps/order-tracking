<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "plan_shopify_starter"),
                'price' => env('PLAN_PRICE_1', 29),
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "plan_shopify_starter"),
                'trial_days' => env('TRIAL_DAY', 30),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 2,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "plan_shopify_premium"),
                'price' => env('PLAN_PRICE_1', 79),
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "plan_shopify_premium"),
                'trial_days' => env('TRIAL_DAY', 30),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 0)
            ],
        ]);
    }
}
