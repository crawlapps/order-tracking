<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddErrorMessageColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->json('error_message')->after('deleted_at')->nullable();
        });

        Schema::table('cybertonica_users', function (Blueprint $table) {
            $table->json('error_message')->after('deleted_at')->nullable();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->json('error_message')->after('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
