<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('cybertonica_status')->after('response')->nullable();
            $table->string('cybertonica_risk')->after('cybertonica_status')->nullable();
            $table->string('cybertonica_risk_score')->after('cybertonica_risk')->default(0);
            $table->string('cybertonica_customer_verification')->after('cybertonica_risk_score')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
