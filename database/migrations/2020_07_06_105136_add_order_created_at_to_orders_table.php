<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderCreatedAtToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('order_created_at')->after('cybertonica_customer_verification')->nullable();
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->unsignedBigInteger('db_order_id')->after('id')->nullable();
            $table->string('order_status')->after('currency')->nullable();

            $table->foreign('db_order_id')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
