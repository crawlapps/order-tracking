<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('order_id')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('order_name')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_zip')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_zip')->nullable();
            $table->double('total_price')->nullable();
            $table->string('currency')->nullable();
            $table->string('financial_status')->nullable();
            $table->boolean('order_confirmed')->nullable();
            $table->string('cancelled_at')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->string('location_id')->nullable();
            $table->string('device_id')->nullable();
            $table->longText('note_attributes')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('gateway')->nullable();
            $table->string('payment_details')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
